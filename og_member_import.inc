<?php

function og_member_import_interface($form_state, $groupnode) {
  $gid = $groupnode->nid;
  drupal_set_title($groupnode->title);
  drupal_set_breadcrumb(drupal_get_breadcrumb());

  $form['import'] =  array(
    '#title' => t('Import users and automatically add them to '.$groupnode->title),
    '#collapsible' => FALSE,
    '#type' => 'fieldset'
  );

  $form['import']['email_list'] =  array(
    '#title' => t('List of email addresses'),
    '#description' => t('Paste the email address one per line into the above, an account will be created and the user notified if no matching account is found.'),
    '#type' => 'textarea'
  );  
  $form['import']['submit'] =  array(
    '#value' => t('Import'),
    '#type' => 'submit'
  );  
    
  return $form;
}

function _og_member_import_email_string_split($str) {
  
  $addies = array();
  foreach( preg_split("/[\r\n]/", $str) as $mail) {
    if( valid_email_address($mail)) {
     $addies[] = $mail;
    }
  }
  return $addies;
}


function og_member_import_interface_validate($element, &$form_state) {  
  $email_addies = _og_member_import_email_string_split($form_state['values']['email_list']);
  if(count($email_addies) == 0) {
    form_set_error('email_list',t('No valid email addresses entered.'));
  }
}


function og_member_import_interface_submit($form, &$form_state) {
  global $user;
  $email_addies = _og_member_import_email_string_split($form_state['values']['email_list']);
  $gid = arg(1);
 
  foreach($email_addies as $mail) {
    // see if this account already exists
    if($uid = db_result(db_query("SELECT uid FROM {users} WHERE mail = '%s'",$mail))) {
      og_save_subscription($gid, $uid, array('is_active' => 1));
      drupal_set_message(t("Added existing member ". $mail));  
    } else {
      $user_pass = user_password();
      
      $user_info = array('init' => $mail,
                         'mail' => $mail, 
                         'name' => $mail,
                         'pass' => $user_pass,
                         'status' => 1,
                         'roles' => array('2'=>true), // default user
                         'timezone' => $user->timezone ); // assume the user is in the same timezone as me.
                         
      $account = user_save('', $user_info, 'account');
      
      // Terminate if an error occured during user_save().
      if (!$account) {
        drupal_set_message(t("Error saving user account ". $mail), 'error');
        $form_state['redirect'] = '';
      } else {
        drupal_set_message(t("Created new account ". $mail));
        // need to create the email addy and then sign them up
        og_save_subscription($gid, $account->uid, array('is_active' => 1));   
      }
    }
  }
}


